'use babel';

import React from 'react';

export class TestCmp2 extends React.Component {
  onClick(e) {
    alert(1);
  }
  render() {
    return (
      <div>test component 2</div>
    );
  }
}

export class TalkPage extends React.Component {
  render() {
    return (
      <div className="uk-panel uk-panel-box uk-panel-box-secondary" id="chatDivWrapper">
        <MsgBox />
        <ChatBar />
      </div>
    );
  }
}



export class MsgBox extends React.Component {
  onClick(e) {
    alert(1);
  }
  render() {
    return (

      <div id="msgBox" className="uk-width-1-1">

      </div>

    );
  }
}

export class ChatBar extends React.Component {
  onClick(e) {
    alert(1);
  }
  render() {
    return (

      <div id="chatBar" className="uk-width-1-1">
        <div className="uk-form">
          <div id="chatBarText">
            <input id="chatBarTextInput" type="text" placeholder="" className="uk-width-1-1 uk-margin-large-right"/>
          </div>
          <div id="chatBarButton">
            <a id="chatBarSendAnchor" className="uk-button uk-button-primary">{this.props.bttext}Send</a>
          </div>
        </div>
      </div>

    );
  }
}
