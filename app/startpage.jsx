'use babel';

// 'use talkpage'

import React from 'react';

import {TalkPage} from './talkpage.jsx'

export class TestCmp extends React.Component {
  handleClick(e) {
    alert(1);
  }
  render () {
    return(
      <div>test component</div>
    );
  }
}

export class Wrapper extends React.Component {
  constructor(props) {
    super(props);

    // alert('in wrapper ctor: this.props.client='+this.props.client)

    // super(props);
    this.state = {
      activeTab: 'login',
      // client: client,
      tag:{'a':1, 'b':2}
    };
    this.activateTab = this.activateTab.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.scheduleNetworkInit = this.scheduleNetworkInit.bind(this);
    // this.registerSubmit = this.registerSubmit.bind(this);

  }

  componentDidMount () {this.scheduleNetworkInit();}

  scheduleNetworkInit () {
    // alert('this.state.client='+this.state.client);
    // alert('this.state.tag='+this.state.tag);
    //store a this ref, and
    var _this = this;
    // alert('_this.state.client='+_this.state.client);

    // this.setState({client:{'aaa':111}});

    // window.requestAnimationFrame(function() {
      // alert('_this.state.activeTab in requestAnimationFrame='+_this.state.activeTab);
      // alert('_this.state.tag["a"] in requestAnimationFrame='+_this.state.tag['a']);
      // var node = _this.getDOMNode();
      // if (node !== undefined) {
      // var net = require('net');
      // console.log('to register: username=' + username);

      // var client = new net.Socket();

      // client.on('data', function(data) {
      //   vnim.netMainLoop(data, vnim.modules['start'].dispatch);
      // });
      // }
    // });
  }

  activateTab (tabId) {
    // alert('tabId=' + tabId);
    this.setState({activeTab: tabId});
  }

  // DON'T REMOVE: a one way parent-child interaction example
  // registerSubmit (username, password) {
  //   var client = this.props.client;
  //   client.write('[REGISTER]\r\nrequester: ' + username + '\r\n' + 'password: ' + password + '\r\ncontent: dummy content\r\n\r\n');
  // }

  render () {
    return(
      <div>

        {this.state.activeTab == 'login'
          ? <LoginForm onActivateTab={this.activateTab} />
          : this.state.activeTab == 'register'
            ? <RegisterForm onActivateTab={this.activateTab} onRegisterSubmit={this.registerSubmit} AppDispatcher={this.props.AppDispatcher}/>
            : null
}


      </div>
    );
  }

}

export class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.toRegister = this.toRegister.bind(this);
  }

  toRegister () {this.props.onActivateTab('register');}

  submit () {React.render(React.createElement(TalkPage, null), document.body);}

  render () {
    return(
      <div className="uk-panel uk-panel-box uk-panel-box-secondary" id="loginFormWrapper">

        <form className="uk-form uk-form-stacked">
          <div className="uk-form-row">
            <label className="uk-form-label" htmlFor="ipLoginUsername">username</label>
            <div className="uk-form-controls">
              <input type="text" id="ipLoginUsername" placeholder="" value="alice"/>
            </div>
          </div>

          <div className="uk-form-row">
            <label className="uk-form-label" htmlFor="ipLoginPassword">password</label>
            <div className="uk-form-controls">
              <input type="password" id="ipLoginPassword" placeholder="" value="111111"/>
            </div>
          </div>

          <div className="uk-form-row">
            <button className="uk-button uk-button-primary" type="button" id="btLogin" onClick={this.submit}>Login</button>
            or
            <a className="link-to-local" onClick={this.toRegister}>register</a>
          </div>
        </form>

      </div>
    );

  }
}

export class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.setState({
      username: 'guest',
      password: 'iamaguest'
    });
    this.toLogin = this.toLogin.bind(this);
    this.submit = this.submit.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);

  }

  toLogin () {this.props.onActivateTab('login');}

  submit () {
    this.props.AppDispatcher.dispatch({
        eventName: 'register',
        data: { username: this.state.username, password: this.state.password } // example data
    });

    // this.props.onRegisterSubmit(this.state.username, this.state.password);
  }

  handleUsernameChange(e){
    this.setState({username: e.target.value});
  }

  handlePasswordChange(e){
    this.setState({password: e.target.value});
  }

  render () {
    return(
      <div className="uk-panel uk-panel-box uk-panel-box-secondary" id="registerFormWrapper">
        <form className="uk-form uk-form-stacked">
          <div className="uk-form-row">
            <label className="uk-form-label" for="ipRegisterUsername">username</label>
            <div className="uk-form-controls">
              <input type="text" id="ipRegisterUsername" placeholder="" onChange={this.handleUsernameChange}/>
            </div>
          </div>

          <div className="uk-form-row">
            <label className="uk-form-label" for="ipRegisterPassword">password</label>
            <div className="uk-form-controls">
              <input type="password" id="ipRegisterPassword" placeholder="" onChange={this.handlePasswordChange}/>
            </div>
          </div>

          <div className="uk-form-row">
            <button className="uk-button uk-button-primary" type="button" id="btRegister" onClick={this.submit}>Register</button>
            or
            <a className="link-to-local" onClick={this.toLogin}>login</a>
          </div>
        </form>
      </div>
    );

  }
}
