module.exports = RegMsg

var Msg = require('./msg'),
    util = require('util');

function RegMsg(username, password){
  Msg.apply(this, arguments);

  this.verb = this.MSGHEADER_VERB_REG
  this.props = {'requester':username, 'password':password}
}

util.inherits(RegMsg, Msg);
