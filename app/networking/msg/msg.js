module.exports = Msg



function Msg(){
  this.MSGHEADER_VERB_UNKNOWN = "UNKNOWN"
  this.MSGHEADER_VERB_CHAT = "CHAT"
  this.MSGHEADER_VERB_LOGIN = "LOGIN"
  this.MSGHEADER_VERB_REG = "REG"
  this.MSGHEADER_VERB_LOGIN_FIN = "LOGIN_FIN"
  this.MSGHEADER_VERB_LOGOUT_FIN = "LOGOUT_FIN"
  this.MSGHEADER_VERB_REG_FIN = "REG_FIN"

  this.EOL = '\r\n'

  this.verb = this.MSGHEADER_VERB_UNKNOWN
  this.props = {}
}

/*
  Msg subtyping requirement:
  this.verb = 'xxx'
  this.props = {'attr1':'val1', 'att2':'val2'}

*/


Msg.prototype.toString = function()  {
  var header = '[' + this.verb + ']'
  var body = ''
  for(var k in this.props) {
    body += k+': '+this.props[k]+this.EOL
  }
  return header + this.EOL + body + this.EOL
  // '[REGISTER]\r\nrequester: ' + username + '\r\n' + 'password: ' + password + '\r\ncontent: dummy content\r\n\r\n'
}
