module.exports = LoginMsg

var Msg = require('./msg'),
    util = require('util');

function LoginMsg(username, password){
  Msg.apply(this, arguments);

  this.verb = this.MSGHEADER_VERB_LOGIN
  this.props = {'username':username, 'password':password}
}

util.inherits(LoginMsg, Msg);
