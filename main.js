'use babel';

import app from 'app';
import BrowserWindow from 'browser-window';

let mainWindow = null;

app.on('window-all-closed', () => {
  if (process.platform != 'darwin') {
    app.quit();
  }
});

app.on('ready', () => {
  mainWindow = new BrowserWindow({width: 360, height: 660});
  mainWindow.loadUrl('file://' + __dirname + '/start.html');
  mainWindow.on('closed', () => {
    mainWindow = null;
  });
});
